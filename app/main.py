from fastapi import FastAPI, HTTPException, Depends
from typing import List
from . import models, database, schemas
from .database import get_db
from sqlalchemy.orm import Session

app = FastAPI()

#Create the tables if they don't exist yet
models.Base.metadata.create_all(bind=database.engine)


############# Implement FastAPI endpoints here ##############

# Create new Task endpoint:



# Get all tasks endpoint:



# Get one task by id endpoint:



# Update one task by id endpoint:



# Delete one task by id endpoint:
